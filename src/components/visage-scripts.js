import * as React from "react"
import { getScriptURLs, scriptKeys } from "@vp/visage-core"
import { stripDomainPrefix } from "../utils/stripDomainPrefix"

export const VisageScripts = ({ canonicalDomain }) => {
  const allScripts = [
    scriptKeys.stylizedDialog,
    scriptKeys.popover,
    scriptKeys.progressiveImage,
  ]
  const options = {
    domain: stripDomainPrefix(canonicalDomain),
  }

  const deferredScriptURLs = getScriptURLs(allScripts, options)

  return (
    <>
      {deferredScriptURLs.map(url => {
        return (
          <script key={url} defer type="text/javascript" src={url}></script>
        )
      })}
    </>
  )
}
