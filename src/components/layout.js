/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import {
  FullHeader, FullFooter
} from '@vp/react-bookends';
import {Visage} from '../components/visage';
import {VisageScripts} from '../components/visage-scripts';
import VistaprintAuth from '@vp/auth';
import { activateAccountMFE } from '@vp/bookends-microfrontends';

import "./layout.css"
import '@vp/react-bookends/dist/styles.css';


const Layout = (props) => {
  const data = useStaticQuery(graphql`
  query SiteTitleQuery {
    bookendsHeader(locale: "en-IE", type: "full")
    bookendsFooter(locale: "en-IE", type: "full")
    site {
      siteMetadata {
        title
      }
    }
  }
  `)
  
  const { bookendsHeader, bookendsFooter } = data
  // This is only an example of how you may provide an auth library to Bookends. Any implementation that leaves you with an `auth` from VistaprintAuth.WebAuth() is acceptable.
  const [auth, setAuthState] = React.useState();
  const [isIdentityInitialized, setIdentityInitialized] = React.useState(false);
  const [identity, setIdentity] = React.useState();

  React.useEffect(() => {
    const developmentMode = true;
    const config = {
      culture: "en-IE",
      developmentMode: developmentMode ? {} : false,
    };

    VistaprintAuth.init(config, identity => {
      const authLibrary = new VistaprintAuth.WebAuth();
      setAuthState(authLibrary);
      activateAccountMFE(authLibrary);
    });
  }, []);

  /* eslint-disable react-hooks/exhaustive-deps*/
  React.useEffect(() => {
    if (auth) {
      setIdentity(auth.getIdentity());

      auth.onUserIdentityUpdate(newIdentity => {
        if (!newIdentity) {
          return;
        }

        if (!isIdentityInitialized) {
          setIdentityInitialized(true);
        }
        if (newIdentity !== identity) {
          setIdentity(newIdentity);
        }
      });
    }
  }, [auth]);

  // if using "Full" size components  
  return (
    <>
            <Visage
          canonicalDomain={"www.vistaprint.ie"}
          visageCoreVersion="v4.2"
        />
      <FullHeader
        {...bookendsHeader}
        auth={auth}
      />
      <main>{props.children}</main>
      <FullFooter {...bookendsFooter} />
      <VisageScripts canonicalDomain={"www.vistaprint.ie"} />
    </>)
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
