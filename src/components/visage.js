import React from "react"
import { Helmet } from "react-helmet"
import { getVisageSetupURLs } from "../utils/getVisageSetupUrls"

export const Visage = ({
  carousel,
  canonicalDomain,
  visageCoreVersion = "v4",
}) => {
  const {
    FONT_FAMILY,
    uiLibraryRelativeUrl,
    fontRegularUrlPath,
    fontMediumUrlPath,
    fontBoldUrlPath,
    styleUrls,
  } = getVisageSetupURLs(canonicalDomain, carousel)

  return (
    <Helmet defer={false}>
      {/* The UI Library default setup https://ui-library.cdn.vpsvc.com/viewer.html#setup */}
      <meta name="viewport" content="initial-scale=1" />
      <meta name="format-detection" content="telephone=no" />
      <meta name="theme-color" content="#006196" />

      <link
        rel="shortcut icon"
        href={`${uiLibraryRelativeUrl}/${visageCoreVersion}/images/favicon/favicon.ico`}
      />
      <link
        rel="apple-touch-icon"
        sizes="152x152"
        href={`${uiLibraryRelativeUrl}/${visageCoreVersion}/images/favicon/vistaprint-favorites-76-76-2014-2x.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="120x120"
        href={`${uiLibraryRelativeUrl}/${visageCoreVersion}/images/favicon/vistaprint-favorites-60-60-2014-2x.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="76x76"
        href={`${uiLibraryRelativeUrl}/${visageCoreVersion}/images/favicon/vistaprint-favorites-76-76-2014.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="60x60"
        href={`${uiLibraryRelativeUrl}/${visageCoreVersion}/images/favicon/vistaprint-favorites-60-60-2014.png`}
      />

      <link
        rel="preload"
        href={`${uiLibraryRelativeUrl}/${fontMediumUrlPath}.woff2`}
        as="font"
        crossOrigin
      />
      <link
        rel="preload"
        href={`${uiLibraryRelativeUrl}/${fontBoldUrlPath}.woff2`}
        as="font"
        crossOrigin
      />
      <link
        rel="preload"
        href={`${uiLibraryRelativeUrl}/${fontRegularUrlPath}.woff2`}
        as="font"
        crossOrigin
      />
      <style type="text/css">
        {` 
          @font-face {           
            font-family: ${FONT_FAMILY};
            font-stretch: normal;
            font-style: normal;
            font-weight: 900; 
            src: url("${uiLibraryRelativeUrl}/${fontBoldUrlPath}.woff2") format("woff2"), url("${uiLibraryRelativeUrl}/${fontBoldUrlPath}.woff") format("woff"); 
          }
          @font-face { 
            font-family: ${FONT_FAMILY};
            font-stretch: normal;
            font-style: normal;
            font-weight: 700; 
            src: url("${uiLibraryRelativeUrl}/${fontMediumUrlPath}.woff2") format("woff2"), url("${uiLibraryRelativeUrl}/${fontMediumUrlPath}.woff") format("woff");  
          }
          @font-face { 
            font-family: ${FONT_FAMILY};
            font-stretch: normal;
            font-style: normal;
            font-weight: 400; 
            src: url("${uiLibraryRelativeUrl}/${fontRegularUrlPath}.woff2") format("woff2"), url("${uiLibraryRelativeUrl}/${fontRegularUrlPath}.woff") format("woff");  
            }`}
      </style>
      {visageStyleIncludes(styleUrls)}
    </Helmet>
  )
}

function visageStyleIncludes(styleUrls) {
  return styleUrls.map(url => {
    return <link key={url} rel="stylesheet" type="text/css" href={url} />
  })
}
