import { getStyleURLs, styleKeys } from "@vp/visage-core"
import { stripDomainPrefix } from "./stripDomainPrefix"
const FONT_FAMILY = "Graphik"
const fontRegularUrlPath = "fonts/Graphik-Regular-Web"
const fontMediumUrlPath = "fonts/Graphik-Medium-Web"
const fontBoldUrlPath = "fonts/Graphik-Bold-Web"

export const getVisageSetupURLs = (canonicalDomain, includeCarousel) => {
  const noUndefined = url => Boolean(url)

  const visageStyleKeys = [
    styleKeys.core,
    styleKeys.popover,
    styleKeys.stylizedDialog,
    styleKeys.textbutton,
    styleKeys.vistagrid,
    styleKeys.spacing,
    styleKeys.accordion,
    styleKeys.progressiveImage,
    styleKeys.controlIcon,
    styleKeys.iconTile,
    styleKeys.promoBar,
    styleKeys.secondaryTile,
    styleKeys.standardHero,
    styleKeys.standardTile,
    styleKeys.detailsBanner,
    styleKeys.embeddedTextHero,
  ]

  const options = {
    domain: stripDomainPrefix(canonicalDomain),
  }
  const styleUrls = getStyleURLs(visageStyleKeys.filter(noUndefined), options)

  const uiLibraryRelativeUrl = `https://${canonicalDomain}/ui-library`

  return {
    FONT_FAMILY,
    uiLibraryRelativeUrl,
    fontRegularUrlPath,
    fontMediumUrlPath,
    fontBoldUrlPath,
    styleUrls,
  }
}
