export const stripDomainPrefix = canonicalDomain => {
  if (typeof canonicalDomain !== "undefined") {
    return canonicalDomain.replace(/^www\./, "")
  }
}
